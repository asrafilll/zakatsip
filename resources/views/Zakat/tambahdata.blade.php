<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <title>Data Zakat</title>
</head>

<body>
  <div class="container">
    <div class="col-lg-2">
      <div class="mb-5 mt-5">
        <a href="/"><button class="btn btn-primary"> Kembali </button></a>
      </div>
    </div>
    <div class="mt-5 col-lg-6">
      <form method="post" action="/tambahdata" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan Nama Asli (tidak boleh kunyah)">
        </div>
        <div class="form-group">
          <label for="status">Status</label>
          <select class="form-control" id="status" name="status">
            <option value="Fakir">Fakir</option>
            <option value="Miskin">Miskin</option>
            <option value="Ibnu Sabil">Ibnu Sabil</option>
            <option value="Amil Zakat">Amil Zakat</option>
            <option value="Fii Sabilillah">Fii Sabilillah</option>
            <option value="Gharim">Gharim</option>
            <option value="Mualaf">Mualaf</option>
          </select>
        </div>
        <div class="form-group">
          <label for="pic">PIC</label>
          <input type="text" name="pic" class="form-control" id="pic" placeholder="Masukkan nama PIC(pengurus sip)">
        </div>
        <div class="form-group">
          <label for="fotopenyerahan">Upload Foto Penyerahan</label>
          <input type="file" class="form-control-file" id="fotopenyerahan" name="fotopenyerahan">
        </div>

        <button type="submit" class="btn btn-success">Tambah Data</button>
      </form>
    </div>
  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>