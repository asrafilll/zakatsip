<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Data Zakat</title>
  </head>
  <body>
    <h1>Hello, world!</h1>
    <div class="row"><h5>Data sudah masuk : {{DB::table('datazakat')->count()}} </h5></div>

<div class="mb-5">
    <a href="/tambahdata"><button class="btn btn-primary"> Tambah Data</button></a>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <table id="table_id" class="table display" style="width: 100%">
                <thead class="thead">
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Asnaf</th>
                        <th>PIC</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $zakat as $zkt )
                    <tr class="row-center">
                        <td>{{$zkt->id}}</td>
                        <td>{{$zkt->name}}</td>
                        <td>{{$zkt->status}}</td>
                        <td>{{$zkt->pic}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
    @stop


    @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">

    <style>
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding: 0px;
            margin-left: 0px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            z-index: 3;
            color: #fff;
            background-color: #121212;
            border-color: #007bff
        }

        .thead {
            background-color: #141414;
            color: #fff;
        }
    </style>








    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>