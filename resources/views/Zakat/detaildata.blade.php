<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Data Zakat</title>
</head>

<body>

    <div class="container">
        
      
            <div class="col-lg-2">
                <div class="mb-5 mt-5">
                    <a href="/"><button class="btn btn-primary"> Kembali </button></a>
                </div>
            </div>
    
        <div class="container">
            <div class="row">
                <div class="col-lg-6 sm-12">
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Nama</th>
                                <th scope="col">asnaf</th>
                                <th scope="col">PIC</th>
                                <th scope="col">Foto</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $zakat2 as $zkt )
                            <tr scope="row">
                                <td>{{$zkt->name}}</td>
                                <td>{{$zkt->status}}</td>
                                <td>{{$zkt->pic}}</td>
                                <td><img style="width:40%;" src="{{ asset(Storage::url($zkt->fotopenyerahan)) }}" />
                                </td>
                                <!-- <td>{{$zkt->fotopenyerahan}}</td> -->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>









    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>