<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;



class ZakatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $zakat = DB::table('datazakat')->orderByDesc('id')->get();
        return view('Zakat/listdata', ['zakat' => $zakat]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Zakat/tambahdata');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


            $data = array();
            $guessExtension = $request->file('fotopenyerahan')->guessExtension();
            $data['name']= $request->get('nama');
            $data['status']  = $request->get('status');
            $data['pic'] = $request->get('pic');
            $data['fotopenyerahan'] = $request->file('fotopenyerahan')->storeAs('public', $request->get('pic').'_'.$request->get('nama'). '.' .$guessExtension);
            $query_insert = DB::table('datazakat')->insert($data);
            
            return redirect('/');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
        $zakat2 = DB::table('datazakat')->where('id', $id)->get();
        return view('Zakat/detaildata', ['zakat2' => $zakat2]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        // $visibility = Storage::getVisibility('image/sss_asda.png');
        $contents = Storage::download('image/sss_asda.png');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
